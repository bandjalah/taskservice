/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package taskservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author XWVS012
 * 
 */
public class Task {
    private int hour; //Hour to start the task
    private int minute; //Minute to start the task
    private String taskCommand; //commandToRunOnTerminal
    private String taskName = null; //OptionalTaskName
    Logger logger;
    
    public Task(int anHour, int aMinute, String aCommand){
        this(anHour,aMinute,aCommand, null);
    }
    
    public Task(int anHour, int aMinute, String aCommand, String aName){
        setHour(anHour);
        setMinute(aMinute);
        setTaskCommand(aCommand);
        setTaskName(aName);
        
        init();
    }
    
    private void init(){
        logger = TaskService.logger;
        
    }
    
    private void setHour(int anHour){
        this.hour = anHour;
    }
    
    private void setMinute(int aMinute){
        this.minute = aMinute;
    }
    
    private void setTaskCommand(String aTaskCommand){
        this.taskCommand = aTaskCommand;
    }
    
    public void setTaskName(String aTaskName){
        this.taskName = aTaskName;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public String getTaskCommand() {
        return taskCommand;
    }
    
    public String getTaskName(){
        return this.taskName == null ? "Annonymous task" : this.taskName;
    }
    
    
    
    public boolean isTimeToRun() {
        boolean itIsTime;
        Date date = new Date();
        
        itIsTime = date.getHours() == getHour() && date.getMinutes() == getMinute();
        
        if(itIsTime){
            logger.info("Time to start "+getTaskName()+" task");
        }else{
            //logger.info("Not yet time to start "+getTaskName()+" task");
            System.out.println("Not yet time to start "+getTaskName()+" task");
        }
        
        return itIsTime;    
    }
    
    
    public void RunTask(){
        logger.info("Starting "+getTaskName()+" task!");
        
        Process p;
        try{
            p = Runtime.getRuntime().exec(getTaskCommand());
            p.waitFor();

            BufferedReader reader = 
             new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";			
            while ((line = reader.readLine())!= null) {
                logger.info(line);
            }
        } catch (Exception ex){
            ex.printStackTrace();
            logger.severe("Ended execution of "+getTaskName());
        }
        logger.info("Ended the execution of "+getTaskName()+" task!");
    }
}
