/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package taskservice;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Timer;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author XWVS012
 */
public class TaskService {

    /**
     * @param args the command line arguments
     */
    public static Logger logger;
    
    public static void main(String[] args) {
        logger = Logger.getLogger("TaskService");
        Date date = new Date();
        try{
            FileHandler fileHandler = new FileHandler("."+File.separator+String.valueOf(date.getYear()+1900)+"-"+String.valueOf(date.getMonth()+1)+"-"+String.valueOf(date.getDate())+"TaskServiceLog.log");
            fileHandler.setFormatter(new SimpleFormatter());
            logger.addHandler(fileHandler);
        }catch (IOException ioException){
            ioException.printStackTrace();
        }
        
        logger.info("Starting timer...");
        //System.out.println("Starting timer");
        
        Timer timer = new Timer();
        
        logger.info("Starting Executor...");
        
        timer.schedule(new ScheduledExecutor(), 0, 60000);
        
        logger.info("Service up and running!");
        
    }
    
}
