/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package taskservice;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author XWVS012
 */
public class ScheduledExecutor extends TimerTask{
    
    private Logger logger;
    private String defaultPath = "."+File.separator+"tasks.conf"; 

    public ScheduledExecutor(){
        logger = TaskService.logger;
    }
    
    @Override
    public void run() {
        //logger.info("Time elapsed, checking tasks...");
        System.out.println("Time elapsed, checking tasks...");
        super.scheduledExecutionTime();
        
        for(Task task : getTasks()){
            if(task.isTimeToRun()){
                task.RunTask();
            }
        }
        
    }
    
    private ArrayList<Task> getTasks(){
        //For mock purposes...
        //logger.info("Mocking tasks!");
        ArrayList<Task> list = new ArrayList<Task>();
        
        try {
            list = readFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //list.add(new Task(0, 0, "sh /etc/mapcrafter/generate", "Minecraft Map Generation"));
        
        return list;        
    }
    
    private ArrayList<Task> readFile() throws IOException{
        ArrayList<Task> list = new ArrayList<Task>();
        File file = new File(this.defaultPath);
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        
        if(!file.exists()){
            logger.severe("Tasks file does not exist! Create a tasks file with this name: tasks.conf");
        }else {
            try {
                fileReader =  new FileReader(file);
                bufferedReader = new BufferedReader(fileReader);
                
                String line = null;
                String [] parameters;
                while(bufferedReader.ready()){
                    line = bufferedReader.readLine();
                    parameters = line.split(",");
                    
                    if(parameters.length == 3){
                        list.add(new Task(Integer.parseInt(parameters[0]), Integer.parseInt(parameters[1]), parameters[2]));
                    } else if (parameters.length == 4){
                        list.add(new Task(Integer.parseInt(parameters[0]), Integer.parseInt(parameters[1]), parameters[2], parameters[3]));
                    }
                }
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ScheduledExecutor.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if(fileReader != null){
                    fileReader.close();
                }
                if(bufferedReader != null){
                    bufferedReader.close();
                }
            }
        }
        
        return list;
    }
        
}
